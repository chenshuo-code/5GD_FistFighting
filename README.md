# 5GD_FistFighting

![unity](https://img.shields.io/badge/Unity-100000?style=for-the-badge&logo=unity&logoColor=white) ![c#](https://img.shields.io/badge/C%23-239120?style=for-the-badge&logo=c-sharp&logoColor=white) ![fmod](https://img.shields.io/badge/FMOD-100000?style=for-the-badge&logo=fmod&logoColor=white) ![itch.io](https://img.shields.io/badge/Itch.io-FA5C5C?style=for-the-badge&logo=itchdotio&logoColor=white)

A student project casual video game in local multiplayer, made during Game Design master course (5th year).

Use your WASD keys or YXAB controller inputs to move with your fists (and wits). Looks too easy?  Beware of other players, but also of the environment! The last one standing will have its chance to be displayed into the main galery.

Time to show them!
